
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1); //hit point value of 1 
	SetCollisionRadius(20); //hit box radius if 20 
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) //if there is a delay or delay has a value other than 0 :cant be negative time then spawn ships
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();//update time that has passed 

		if (m_delaySeconds <= 0)  //if delay is 0 or in other words if there is no delay then active ship
		{
			GameObject::Activate(); //starts ship
		}
	}

	if (IsActive()) //following line 20 if it is active then set activation seconds to time that has passed.
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); //otherwise eliminate method
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}