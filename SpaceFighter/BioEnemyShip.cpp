
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150); //movement speed set to an int of 150
	SetMaxHitPoints(1); //int set to 1 value
	SetCollisionRadius(20); //sets a radius of 20 for hitbox
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive()) //checks that game is running 
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); //caculates x by taking time that has passed since game has started times pi 
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;  
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed()); //moves position of ship based on passsed game time.

		if (!IsOnScreen()) Deactivate(); //if its already on screen eliminate method
	}

	EnemyShip::Update(pGameTime); //updates game time 
}


 void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
