

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships

	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\MyEnemyShip.png");

	const int COUNT = 60;

	double xPositions[COUNT] =
	{
		0.10, 0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55, 0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55, 0.5, 0.4, 0.6,  0.7, 0.4, 0.6,  0.7, 0.25, 0.35, 0.2, 0.4
	};
	
	double delays[COUNT] =
	{
		0.0, 0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3, 7.0, .5, .5, .5, 
		.5, .3,.3,.3,.3,.3, .5, .5, .5, .5, .5, .5, 0.3, 0.3, 0.3, 7.0, .5, .5, .5,
		.5, .3, .1, .1, .1, .3, .1, .1, .1, 0.3, 0.3, 0.3
	};


	Texture* bpTexture = pResourceManager->Load<Texture>("Textures\\BossShip.png");

	const int BOSS = 1;

	double bxPositions[BOSS] =
	{
		0.9
	};

	double bdelays[COUNT] =
	{
		5.0
	};


	float bdelay = 0.05; // start delay
	Vector2 bposition;

	for (int i = 0; i < BOSS; i++)
	{
		bdelay += bdelays[i];
		bposition.Set(bxPositions[i] * Game::GetScreenWidth(), -bpTexture->GetCenter().Y);

		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(bpTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(bposition, (float)bdelay);
		AddGameObject(pEnemy);
	}





	float delay = 0.05; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();		
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

