
#include "Projectile.h"

Texture *Projectile::s_pTexture = nullptr;

Projectile::Projectile()
{
	SetSpeed(500); //sets movement speed of sprite 
	SetDamage(1); // affect of projectile has a value set to 1
	SetDirection(-Vector2::UNIT_Y); //sets direction staight up relatively speaking. can only go from location of player ship in a upward direction
	SetCollisionRadius(12); // hitbox radius set to 9

	m_drawnByLevel = true;
}

void Projectile::Update(const GameTime *pGameTime)
{
	if (IsActive()) //if game has started
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed(); //gets location of player ship with speed and direction it has moved since the game time pas passed
		TranslatePosition(translation); //moves location 

		Vector2 position = GetPosition();
		Vector2 size = s_pTexture->GetSize(); //grabs size of texture that was set

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate(); 
		else if (position.X < -size.X) Deactivate();  //
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}

	GameObject::Update(pGameTime);
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}